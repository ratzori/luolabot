#include "luolabot.h"
#include "ui_luolabot.h"

#include "bot.h"
#include "connectmanager.h"
#include "clientparser.h"

#include <QMessageBox>
#include <QHBoxLayout>
#include <QPushButton>

LuolaBot::LuolaBot(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LuolaBot)
{
    ui->setupUi(this);
    ui->healthBar->setMinimum(0);

    conn = new ConnectManager(this);
    ClientParser *parser = new ClientParser(this);
    Bot *bot = new Bot(this);

    connect( conn, SIGNAL(connected()), this, SLOT(connected()) );
    connect( conn, SIGNAL(disconnected()), this, SLOT(disconnected()) );

    connect( conn, SIGNAL(latency(QString)), ui->pingEdit, SLOT(setText(QString)));

    connect( parser, SIGNAL(jsonRequest(QJsonDocument&)), conn, SLOT(jsonWrite(QJsonDocument&)) );
    connect( conn, SIGNAL(jsonReceived(QJsonDocument&)), parser, SLOT(jsonResp(QJsonDocument&)) );
    connect( parser, SIGNAL(serverError(QString&)), this, SLOT(serverError(QString&)) );

    connect( bot, SIGNAL(requestJoin(QString&,QString&)), parser, SLOT(reqJoinHandler(QString&,QString&)) );
    connect( parser, SIGNAL(botJoined(quint64)), bot, SLOT(botJoined(quint64)) );

    connect( bot, SIGNAL(requestFOW(quint64)), parser, SLOT(reqFOWHandler(quint64)) );
    connect( parser, SIGNAL(fullFOW(QString&,quint8,quint8,QString&)), bot, SLOT(fullFOW(QString&,quint8,quint8,QString&)) );

    connect( bot, SIGNAL(requestMove(quint64,QString&)), parser, SLOT(reqMoveHandler(quint64,QString&)) );
    connect( bot, SIGNAL(requestRotate(quint64,QString&)), parser, SLOT(reqRotateHandler(quint64,QString&)) );
    connect( bot, SIGNAL(requestInteract(quint64,quint64)), parser, SLOT(reqInteractHandler(quint64,quint64)) );
    connect( bot, SIGNAL(requestAttack(quint64)), parser, SLOT(reqAttackHandler(quint64)));

    connect( parser, SIGNAL(partialFOW(QString&,quint8,quint8,QString&)), bot, SLOT(partialFOW(QString&,quint8,quint8,QString&)) );
    connect( parser, SIGNAL(fullFOW(QString&,quint8,quint8,QString&)), this, SLOT(fowData(QString&,quint8,quint8,QString&)) );

    connect( bot, SIGNAL(botMaxHealth(int)), ui->healthBar, SLOT(setMaximum(int)));
    connect( bot, SIGNAL(botHealth(int)), ui->healthBar, SLOT(setValue(int)));
    connect( parser, SIGNAL(botCharacterStatus(QString&,quint32)), bot, SLOT(botCharacterStatus(QString&,quint32)));

    connect( this, SIGNAL(joinRequest(QString&,QString&)), bot, SLOT(join(QString&,QString&)) );
    connect( this, SIGNAL(moveRequest(QString&)), bot, SLOT(move(QString&)) );
    connect( this, SIGNAL(rotationRequest(QString&)), bot, SLOT(rotate(QString&)) );
    connect( this, SIGNAL(interactRequest(quint64)), bot, SLOT(interact(quint64)) );

    connect( bot, SIGNAL(viewFrontUpdated(QString)), ui->viewEdit, SLOT(setText(QString)) );

    connect( this, SIGNAL(aiActivateRequest()), bot, SLOT(aiActivate()) );
    connect( this, SIGNAL(aiDeactivateRequest()), bot, SLOT(aiDeactivate()) );
    connect( bot, SIGNAL(aiActived()), this, SLOT(aiActivated()) );
    connect( bot, SIGNAL(aiDeactivated()), this, SLOT(aiDeactivated()) );

    assignShortcuts();

    _aiActivated = false;
    connectionOpen = false;
}

LuolaBot::~LuolaBot()
{
    delete ui;
}


void LuolaBot::assignShortcuts(void)
{
    QAction *forward = new QAction(this);
    forward->setShortcut(Qt::Key_W);
    connect(forward, SIGNAL(triggered()), this, SLOT(on_buttonForward_clicked()));
    this->addAction(forward);

    QAction *backward = new QAction(this);
    backward->setShortcut(Qt::Key_S);
    connect(backward, SIGNAL(triggered()), this, SLOT(on_buttonBackward_clicked()));
    this->addAction(backward);

    QAction *left = new QAction(this);
    left->setShortcut(Qt::Key_A);
    connect(left, SIGNAL(triggered()), this, SLOT(on_buttonLeft_clicked()));
    this->addAction(left);

    QAction *right = new QAction(this);
    right->setShortcut(Qt::Key_D);
    connect(right, SIGNAL(triggered()), this, SLOT(on_buttonRight_clicked()));
    this->addAction(right);

    QAction *north = new QAction(this);
    north->setShortcut(Qt::Key_Up);
    connect(north, SIGNAL(triggered()), this, SLOT(on_buttonNorth_clicked()));
    this->addAction(north);

    QAction *east = new QAction(this);
    east->setShortcut(Qt::Key_Right);
    connect(east, SIGNAL(triggered()), this, SLOT(on_buttonEast_clicked()));
    this->addAction(east);

    QAction *south = new QAction(this);
    south->setShortcut(Qt::Key_Down);
    connect(south, SIGNAL(triggered()), this, SLOT(on_buttonSouth_clicked()));
    this->addAction(south);

    QAction *west = new QAction(this);
    west->setShortcut(Qt::Key_Left);
    connect(west, SIGNAL(triggered()), this, SLOT(on_buttonWest_clicked()));
    this->addAction(west);

    QAction *interact = new QAction(this);
    interact->setShortcut(Qt::Key_I);
    connect(interact, SIGNAL(triggered()), this, SLOT(on_buttonInteract_clicked()));
    this->addAction(interact);
}

void LuolaBot::fowData(QString &orientation, quint8 width, quint8 height, QString &pattern)
{
    if ( orientation == "north" )
    {
        ui->orientation->setValue(180);
    }
    else if ( orientation == "east" )
    {
        ui->orientation->setValue(270);
    }
    else if ( orientation == "south" )
    {
        ui->orientation->setValue(0);
    }
    else if ( orientation == "west" )
    {
        ui->orientation->setValue(90);
    }

    QString rendered("");

    for ( int i = 0; i < height; i++ )
    {
        for ( int j = 0; j < width; j++ )
        {
        rendered.append(pattern.at( i * width + j ) );
        }

        rendered.append("<br />");
    }

    ui->textEdit->setHtml(rendered);
}

void LuolaBot::connected()
{
    ui->connectButton->setText("Disconnect");
    ui->textEdit->setText("Connected");

    connectionOpen = true;
}

void LuolaBot::disconnected()
{
    ui->connectButton->setText("Connect");
    ui->textEdit->setText("Disconnected");

    connectionOpen = false;
}

void LuolaBot::aiActivated(void)
{
    _aiActivated = true;
    ui->aiButton->setText("AI deact.");
}

void LuolaBot::aiDeactivated(void)
{
    _aiActivated = false;
    ui->aiButton->setText("AI act.");
}

void LuolaBot::on_buttonLeft_clicked()
{
    QString direction("left");
    emit moveRequest(direction);
}

void LuolaBot::on_buttonForward_clicked()
{
    QString direction("forward");
    emit moveRequest(direction);
}

void LuolaBot::on_buttonRight_clicked()
{
    QString direction("right");
    emit moveRequest(direction);
}

void LuolaBot::on_buttonBackward_clicked()
{
    QString direction("backward");
    emit moveRequest(direction);
}

void LuolaBot::on_buttonNorth_clicked()
{
    QString orientation("north");
    emit rotationRequest(orientation);
}

void LuolaBot::on_buttonEast_clicked()
{
    QString orientation("east");
    emit rotationRequest(orientation);
}

void LuolaBot::on_buttonSouth_clicked()
{
    QString orientation("south");
    emit rotationRequest(orientation);
}

void LuolaBot::on_buttonWest_clicked()
{
    QString orientation("west");
    emit rotationRequest(orientation);
}

void LuolaBot::on_buttonJoin_clicked()
{
    QString nick("Desding");
    QString password("Desting");

    emit joinRequest(nick, password);
}

void LuolaBot::on_buttonQuit_clicked()
{

}

void LuolaBot::on_buttonInteract_clicked()
{
    quint64 targetID = 0;

    emit interactRequest(targetID);
}

void LuolaBot::on_connectButton_clicked()
{
    if ( !connectionOpen )
    {
        conn->Connect( ui->hostNameEdit->text(), ui->hostPortEdit->text().toShort());
    }
    else
    {
        conn->Disconnect();
    }
}

void LuolaBot::on_aiButton_clicked()
{
    if ( !_aiActivated )
    {
        emit aiActivateRequest();
    }
    else
    {
        emit aiDeactivateRequest();
    }
}

void LuolaBot::serverError(QString &error)
{
    QString errorMessage("The server responded: \"");
    errorMessage.append(error);
    errorMessage.append("\"");

    QMessageBox::warning(this, tr("Error"),
                           errorMessage,
                           QMessageBox::Ok);
}
