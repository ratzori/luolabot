#include "clientparser.h"

#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>

ClientParser::ClientParser(QObject *parent) : QObject(parent)
{

}

void ClientParser::reqJoinHandler(QString &nick, QString &password)
{
    _nick = nick;
    _password = password;

    QJsonObject request;

    request.insert(QString("type"), QString("req_join"));

    QJsonObject from;
    from.insert(QString("nick"), nick);
    from.insert(QString("password"), password);

    request.insert(QString("from"), from);

    QJsonDocument jsonReq(request);

    emit jsonRequest(jsonReq);
}

void ClientParser::reqFOWHandler(quint64 id)
{
    QJsonObject request;
    request.insert(QString("type"), QString("req_fow"));

    QJsonObject from;
    from.insert(QString("id"),(qint64)id);
    from.insert(QString("nick"), _nick);
    from.insert(QString("password"), _password);
    request.insert(QString("from"), from);

    QJsonDocument jsonReq(request);

    emit jsonRequest(jsonReq);
}

void ClientParser::reqMoveHandler(quint64 id, QString &direction)
{
    QJsonObject request;

    request.insert(QString("type"), QString("req_action"));

    QJsonObject from;
    from.insert(QString("id"),(qint64)id);
    from.insert(QString("nick"), _nick);
    from.insert(QString("password"), _password);
    request.insert(QString("from"), from);

    QJsonObject move;
    move.insert(QString("direction"), direction);

    QJsonObject reqAction;

    reqAction.insert(QString("action_type"), QString("move"));
    reqAction.insert(QString("move"), move);
    request.insert(QString("req_action"), reqAction);

    QJsonDocument jsonReq(request);

    emit jsonRequest(jsonReq);
}

void ClientParser::reqRotateHandler(quint64 id, QString &orientation)
{
    QJsonObject request;

    request.insert(QString("type"), QString("req_action"));

    QJsonObject from;
    from.insert(QString("id"),(qint64)id);
    from.insert(QString("nick"), _nick);
    from.insert(QString("password"), _password);
    request.insert(QString("from"), from);

    QJsonObject rotate;
    rotate.insert(QString("orientation"), orientation);

    QJsonObject reqAction;
    reqAction.insert(QString("action_type"), QString("rotate"));
    reqAction.insert(QString("rotate"), rotate);
    request.insert(QString("req_action"), reqAction);

    QJsonDocument jsonReq(request);

    emit jsonRequest(jsonReq);
}

void ClientParser::reqInteractHandler(quint64 id, quint64 targetID)
{
    QJsonObject request;

    request.insert(QString("type"), QString("req_action"));

    QJsonObject from;
    from.insert(QString("id"),(qint64)id);
    from.insert(QString("nick"), _nick);
    from.insert(QString("password"), _password);
    request.insert(QString("from"), from);

    QJsonObject interact;
    interact.insert(QString("id"), (qint64)targetID);

    QJsonObject reqAction;
    reqAction.insert(QString("action_type"), QString("interact"));
    reqAction.insert(QString("interact"), interact);
    request.insert(QString("req_action"), reqAction);

    QJsonDocument jsonReq(request);

    emit jsonRequest(jsonReq);
}

void ClientParser::reqAttackHandler(quint64 id)
{
    QJsonObject request;

    request.insert(QString("type"), QString("req_action"));

    QJsonObject from;
    from.insert(QString("id"),(qint64)id);
    from.insert(QString("nick"), _nick);
    from.insert(QString("password"), _password);

    QJsonObject reqAction;
    reqAction.insert(QString("from"), from);
    reqAction.insert(QString("action_type"), QString("attack"));
    request.insert(QString("req_action"), reqAction);

    QJsonDocument jsonReq(request);

    emit jsonRequest(jsonReq);
}

void ClientParser::keyHandler(QJsonObject &key)
{
    qDebug() << "Key";
    quint64 id = key.value(QString("id")).toInt();
}

void ClientParser::weaponHandler(QJsonObject &weapon)
{
    qDebug() << "Weapon";

    quint64 id = weapon.value(QString("id")).toInt();
    QString type = weapon.value(QString("type")).toString();
    quint32 damage = weapon.value(QString("damage")).toInt();
}

void ClientParser::bombHandler(QJsonObject &bomb)
{
    qDebug() << "Bomb";

    quint64 id = bomb.value(QString("id")).toInt();
}

void ClientParser::itemHandler(QJsonObject &item)
{
    QString type = item.value(QString("type")).toString();

    if ( type == "key" )
    {
        QJsonObject key = item.value(QString("key")).toObject();
        keyHandler(key);
    }
    else if ( type == "weapon" )
    {
        QJsonObject weapon = item.value(QString("weapon")).toObject();
        weaponHandler(weapon);
    }
    else if ( type == "bomb" )
    {
        QJsonObject bomb = item.value(QString("bomb")).toObject();
        bombHandler(bomb);
    }
}

void ClientParser::visibleItemsHandler(QJsonArray &inventory)
{
    //qDebug() << inventory.size();

    for ( int i = 0; i < inventory.size(); i++ )
    {
        QJsonObject item = inventory.at(i).toObject();
        itemHandler(item);
    }

}

void ClientParser::humanHandler(QJsonObject &human)
{
    qDebug() << "human";

    quint64 id = human.value(QString("id")).toInt();
    QString name = human.value(QString("type")).toString();
    QString status = human.value(QString("type")).toString();
}

void ClientParser::characterHandler(QJsonObject &character)
{
    QString type = character.value(QString("type")).toString();

    if ( type == "human" )
    {
        QJsonObject human = human.value(QString("type")).toObject();
        humanHandler(human);
    }
    else
    {
        qDebug() << "Unknown character:" << type;
    }
}

void ClientParser::visibleCharactersHandler(QJsonArray &characters)
{
    //qDebug() << characters.size();

    for ( int i = 0; i < characters.size(); i++ )
    {
        QJsonObject character = characters.at(i).toObject();
        characterHandler(character);
    }

}

void ClientParser::sightDataHandler(QJsonObject &sightData)
{
    //qDebug() << "Parser: Sight data received";

    QString type = sightData.value(QString("type")).toString();
    QString orientation = sightData.value(QString("orientation")).toString();

    quint8 width = sightData.value(QString("width")).toInt();
    quint8 height = sightData.value(QString("height")).toInt();

    QString pattern = sightData.value(QString("pattern")).toString();

    if ( type == "partial" )
    {
        if ( sightData.contains(QString("visible_items")) )
        {
            QJsonArray visibleItems = sightData.value(QString("visible_items")).toArray();
            visibleItemsHandler( visibleItems );
        }
        if ( sightData.contains(QString("visible_characters")) )
        {
            QJsonArray visibleCharacters = sightData.value(QString("visible_characters")).toArray();
            visibleCharactersHandler( visibleCharacters );
        }

        emit partialFOW(orientation, width, height, pattern);
    }
    else if ( type == "full" )
    {
        emit fullFOW(orientation, width, height, pattern);
    }
    else
    {
        // Unknown sight type
    }
}

void ClientParser::inventoryHandler(QJsonArray &inventory)
{
    //qDebug() << inventory.size();

    for ( int i = 0; i < inventory.size(); i++ )
    {
        QJsonObject item = inventory.at(i).toObject();
        itemHandler(item);
    }

}

void ClientParser::ownCharacterHandler(QJsonObject &character)
{
    QString status = character.value(QString("status")).toString();
    quint32 health = character.value(QString("health")).toInt();

    emit botCharacterStatus( status, health );

    QJsonArray inventory = character.value(QString("inventory")).toArray();
    inventoryHandler(inventory);
}

void ClientParser::respJoinHandler(QJsonObject &respJoin)
{
    quint64 id = respJoin.value(QString("id")).toInt();
    QString error = respJoin.value(QString("error")).toString();

    if ( error == "" )
    {
        emit botJoined(id);
    }
    else
    {
        qDebug() << "Join resp:" << error;
    }
}

void ClientParser::respQuitHandler(QJsonObject &respQuit)
{
    quint64 id = respQuit.value(QString("id")).toInt();
    QString error = respQuit.value(QString("error")).toString();

    if ( error != "" )
    {
        qDebug() << "Resp quit: " << id << error;
    }

}

void ClientParser::respFOWHandler(QJsonObject &respFOW)
{
    quint64 id = respFOW.value(QString("id")).toInt();

    if ( respFOW.contains(QString("sight_data")) )
    {
        QJsonObject sightData = respFOW.value(QString("sight_data")).toObject();
        sightDataHandler( sightData );
    }
}

void ClientParser::respStatusHandler(QJsonObject &respStatus)
{
    quint64 id = respStatus.value(QString("id")).toInt();

    if ( respStatus.contains(QString("sight_data")) )
    {
        QJsonObject sightData = respStatus.value(QString("sight_data")).toObject();
        sightDataHandler(sightData);
    }

    if ( respStatus.contains(QString("character")) )
    {
        QJsonObject character = respStatus.value(QString("character")).toObject();
        ownCharacterHandler(character);
    }
}

void ClientParser::errorHandler(QJsonObject &error)
{
    QString errorMessage = error.value(QString("message")).toString();

    emit serverError(errorMessage);
}

void ClientParser::jsonResp(QJsonDocument &jsonDoc)
{
    QJsonObject msg = jsonDoc.object();
    QString msgType = msg.value(QString("type")).toString();
    QJsonObject resp;

    //qDebug() << "Msg handler:" << msgType;

    if ( msgType == "resp_join" )
    {
        resp = msg.value("resp_join").toObject();
        respJoinHandler(resp);
    }
    else if ( msgType == "resp_quit" )
    {
        resp = msg.value("resp_quit").toObject();
        respQuitHandler(resp);
    }
    else if ( msgType == "resp_fow" )
    {
        resp = msg.value("resp_fow").toObject();
        respFOWHandler(resp);
    }
    else if ( msgType == "resp_status" )
    {
        resp = msg.value("resp_status").toObject();
        respStatusHandler(resp);
    }
    else if ( msgType == "resp_action" )
    {
        resp = msg.value("resp_action").toObject();
        respStatusHandler(resp);
    }
    else if ( msgType == "error" )
    {
        resp = msg.value("error").toObject();
        errorHandler(resp);
    }
}
