#ifndef CONNECTMANAGER_H
#define CONNECTMANAGER_H

#include <QObject>
#include <QNetworkSession>
#include <QDialog>
#include <QTcpSocket>
#include <QDataStream>
#include <QJsonDocument>
#include <QElapsedTimer>
#include <QTimer>

class ConnectManager : public QObject
{
    Q_OBJECT

public:
    explicit ConnectManager(QObject *parent = Q_NULLPTR);

public slots:
    void Connect(QString hostName, quint16 port);
    void Disconnect(void);
    void jsonWrite(QJsonDocument &jsonReq);

private slots:
    void readTcpData(void);
    void socketConnected(void);
    void socketDisconnected(void);
    void errorHandler(QAbstractSocket::SocketError socketError);
    void timeout(void);

signals:
    void jsonReceived(QJsonDocument &jsonResp);
    void latency(QString latencyValue);
    void connected(void);
    void disconnected(void);

private:
    QTcpSocket *tcpSocket;
    QDataStream in;
    QByteArray rawData;
    QJsonDocument jsonResp;
    QElapsedTimer timer;
    QTimer connOpenTimer;
    bool waitingData;
};

#endif // CONNECTMANAGER_H
