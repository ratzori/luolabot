#include "bot.h"

#include <QtMath>
#include <QTimer>
#include <QDebug>

Bot::Bot(QObject *parent) : QObject(parent)
{
    botState.append(BotState::Init);
    id = 0;
    health = 0;
    characterStatus.clear();

    myPosition.setX(0);
    myPosition.setY(0);
}

void Bot::join(QString &nick, QString &password)
{
    _nick = nick;
    _password = password;

    emit requestJoin(nick, password);
}

void Bot::move(QString &direction)
{
    emit requestMove(id, direction);
}

void Bot::rotate(QString &orientation)
{
    emit requestRotate(id, orientation);
}

int Bot::orientationToDegree(QString &orientation)
{
    if ( orientation == "north" )
    {
        return 0;
    }
    else if ( orientation == "east" )
    {
        return 90;
    }
    else if ( orientation == "south" )
    {
        return 180;
    }
    else if ( orientation == "west" )
    {
        return 270;
    }
    else if ( orientation == "right" )
    {
        return ( _orientationDegree + 90 ) % 360;
    }
    else if ( orientation == "left" )
    {
        if ( ( _orientationDegree - 90 ) >= 0 )
        {
            return _orientationDegree - 90;
        }
        else
        {
            return 360 - 90;
        }
    }
    else if ( orientation == "backwards" )
    {
        return ( _orientationDegree + 180 ) % 360;
    }

    qDebug() << "Unknown orientation";
    return 0;
}

QString Bot::degreeToOrientation(int degree)
{
    QString orientation("");

    if ( degree < 0 )
    {
        degree = 360 - degree;
    }

    degree = degree % 360;

    switch ( degree )
    {
    case 0:
        orientation.append("north");
        break;
    case 90:
        orientation.append("east");
        break;
    case 180:
        orientation.append("south");
        break;
    case 270:
        orientation.append("west");
        break;
    default:
        orientation.append("north");
        qDebug() << "Can't convert degree to orientation";
        break;
    }

    return orientation;
}

void Bot::rotate(int degree)
{
    QString orientation = degreeToOrientation(degree);

    emit requestRotate(id, orientation);
}

void Bot::interact(quint64 targetID)
{
    emit requestInteract(id, targetID);
}


void Bot::attack(void)
{
    emit requestAttack(id);
}

void Bot::botJoined(quint64 botID)
{
    qDebug() << "Bot: my ID:" << botID;
    id = botID;

    //currentState.append(BotState::Connected);

    emit requestFOW(id);
}

QChar Bot::posAt( QPoint &point )
{
    return fullPattern.at( point.y() * fullPatternSize.x() + point.x() );
}

QString Bot::blockDetector( QPoint point )
{
    QChar viewBlock = posAt(point);

    if ( viewBlock == "." )
    {
        return "Terrain";
    }
    else if ( viewBlock == "#" )
    {
        return "Wall";
    }
    else if ( viewBlock == "%" )
    {
        return "Food";
    }
    else if ( viewBlock == "+" )
    {
        return "Closed door";
    }
    else if ( viewBlock == "-" )
    {
        return "Open door";
    }
    else if ( viewBlock == "P" )
    {
        return "Person";
    }
    else if ( viewBlock == "S" )
    {
        return "Shadow";
    }
    else
    {
        qDebug() << "Unknown object in view: " << viewBlock;
        return "";
    }
}

void Bot::viewDetector()
{
    frontView = blockDetector(frontPos);
    backView = blockDetector(backPos);
    rightView = blockDetector(rightPos);
    leftView = blockDetector(leftPos);

    rightBackView = blockDetector(rightBackPos);

    emit viewFrontUpdated(frontView);
}

QPoint Bot::degreeToBlockOffset( int degrees )
{
    /* Offset to block in front */
    int x = qRound(qSin(qDegreesToRadians(static_cast<qreal>(degrees))));
    int y = qRound(qCos(qDegreesToRadians(static_cast<qreal>(degrees))));
    y *= -1; /* Y is opposite in the view array */

    return QPoint(x,y);
}

void Bot::viewConverter()
{
    frontPos = botPos + degreeToBlockOffset( _orientationDegree );
    backPos = botPos + degreeToBlockOffset( _orientationDegree + 180 );
    rightPos = botPos + degreeToBlockOffset( _orientationDegree + 90 );
    leftPos = botPos + degreeToBlockOffset( _orientationDegree + 270 );

    rightBackPos = botPos + degreeToBlockOffset( _orientationDegree + 135 );
}

void Bot::fullFOW(QString &orientation, quint8 width, quint8 height, QString &pattern)
{
    //qDebug() << "Bot: full FOW:" << orientation << width << height << pattern;

    if ( ( width > 100 ) || ( height > 100 ) )
    {
        qDebug() << "Quite big fow...";
        return;
    }

    botPos.setX(( width - 1 ) / 2);
    botPos.setY(( height - 1 ) / 2);

    fullPatternSize.setX(width);
    fullPatternSize.setY(height);

    fullPattern = pattern;

    _orientation = orientation;


    int newOrientationDegree = orientationToDegree(orientation);

    if ( _orientationDegree != newOrientationDegree )
    {
        _orientationDegree = newOrientationDegree;
        viewConverter();
    }

    viewDetector();

    aiTurnOpportunity(100);
}

void Bot::partialFOW(QString &orientation, quint8 width, quint8 height, QString &pattern)
{
    if ( ( width > 100 ) || ( height > 100 ) )
    {
        qDebug() << "Quite big fow...";
        return;
    }

    _orientation = orientation;
    partialPatternSize.setX(width);
    partialPatternSize.setY(height);

    partialPattern = pattern;
    qDebug() << partialPattern;
}

void Bot::botCharacterStatus(QString &newStatus, quint32 newHealth)
{
    if ( characterStatus != newStatus )
    {
        characterStatus = newStatus;
        emit botStatus(characterStatus);
    }
    if ( health == 0 )
    {
        emit botMaxHealth((int)newHealth);
    }
    if ( health != newHealth )
    {
        health = newHealth;
        emit botHealth(health);
    }
}

void Bot::aiActivate(void)
{
    _aiActived = true;
    emit aiActived();

    botState.append(BotState::InitScan);

    aiTurnOpportunity(0);
}

void Bot::aiDeactivate(void)
{
    _aiActived = false;
    botState.clear();
    emit aiDeactivated();
}

void Bot::aiTurnOpportunity(int time)
{
    if ( _aiActived )
    {
        QTimer::singleShot(time, this, SLOT(UltimateBrainUnit()));
    }
}

bool Bot::isMoveableArea(QString block)
{
    if ( ( block == "Terrain" ) || ( block == "Food" ) || ( block == "Open door" ) )
    {
        return true;
    }
    return false;
}

void Bot::UltimateBrainUnit(void)
{
    if  ( !_aiActived )
    {
        return;
    }

    QString direction = "forward";
    QString orientation = "";

    if (  botState.isEmpty() )
    {
        Q_ASSERT(false);
        qDebug() << "Command list empty";
        return;
    }

    BotState currentState = botState.first();
    botState.remove(0);

    qDebug() << currentState;

    if ( currentState == BotState::Init )
    {
        botState.append(BotState::InitScan);
        aiTurnOpportunity(1000);
    }
    else if ( currentState == BotState::InitScan )
    {
        botState.append(BotState::scanOngoing);

        qDebug() << "Init scan";
        rotate(0);
    }
    else if ( currentState == BotState::scanOngoing )
    {
        qDebug() << "Scan ongoing";

        if ( ( ( _orientationDegree + 90 ) % 360 ) == 0 )
        {
            botState.append(BotState::decideAction);
            qDebug() << "Scan complete";
        }
        else
        {
            botState.append(BotState::scanOngoing);
        }

        rotate( _orientationDegree + 90 );

    }
    else if ( currentState == BotState::decideAction )
    {
        /*
        qDebug() << "Front" << frontView;
        qDebug() << "Back" << backView;
        qDebug() << "Right" << rightView;
        qDebug() << "Left" << leftView;*/

        if ( frontView == "Person" )
        {
            qDebug() << "Attacking";
            attack();
            botState.append(BotState::decideAction);
        }
        else if ( backView == "Open door" )
        {
             qDebug() << "Door seems to be open behind me";
             orientation = "backwards";

             rotate(orientationToDegree(orientation));
             botState.append(BotState::closeDoor);
             botState.append(BotState::turnBackwards);
             botState.append(BotState::decideAction);
        }
        else if ( frontView == "Closed door" )
        {
            qDebug() << "Opening door";
            interact(0);
            botState.append(BotState::goForward);
            botState.append(BotState::decideAction);
        }
        else if ( rightView == "Closed door" )
        {
            qDebug() << "Closed door on right";
            orientation = "right";
            rotate(orientationToDegree(orientation));
            qDebug() << "Turning right";
            botState.append(BotState::openDoor);
            botState.append(BotState::goForward);
            botState.append(BotState::decideAction);
        }
        else if ( isMoveableArea(frontView) )
        {
            if ( isMoveableArea(backView) && isMoveableArea(rightView) && isMoveableArea(leftView) &&
                 isMoveableArea(rightBackView) )
            {
                qDebug() << "Open area, moving forward";
                direction = "forward";
                move(direction);
                botState.append(BotState::decideAction);
            }
            else if ( isMoveableArea(rightView) )
            {
                qDebug() << "Seems that forward and right are free";
                qDebug() << "Rotating to right and next moving forward";
                orientation = "right";
                rotate(orientationToDegree(orientation));
                botState.append(BotState::goForward);
                botState.append(BotState::decideAction);
            }
            else
            {
                qDebug() << "Going forward";
                direction = "forward";
                move(direction);
                botState.append(BotState::decideAction);
            }
        }
        else if ( isMoveableArea(rightView) && !isMoveableArea(rightBackView) )
        {
            qDebug() << "Right is free";
            qDebug() << "Rotating to right and next moving forward";
            orientation = "right";
            rotate(orientationToDegree(orientation));
            botState.append(BotState::goForward);
            botState.append(BotState::decideAction);
        }
        else
        {
            qDebug() << "Turning left";
            orientation = "left";
            rotate(orientationToDegree(orientation));
            botState.append(BotState::decideAction);
        }
    }
    else if ( currentState == BotState::closeDoor )
    {
        if ( frontView == "Open door" )
        {
            qDebug() << "Closing door";
            interact(0);
        }
        else
        {
            qDebug() << "Door seems to be already closed";
        }
    }
    else if ( currentState == BotState::openDoor )
    {
        if ( frontView == "Closed door" )
        {
            qDebug() << "Opening door";
            interact(0);
        }
        else
        {
            qDebug() << "Door seems to be already open";
        }
    }
    else if ( currentState == BotState::turnRight )
    {
        qDebug() << "Turning right";
        orientation = "right";
        rotate(orientationToDegree(orientation));
    }
    else if ( currentState == BotState::turnBackwards )
    {
        qDebug() << "Turning backwards";
        orientation = "backwards";
        rotate(orientationToDegree(orientation));
    }
    else if ( currentState == BotState::goForward )
    {
        qDebug() << "Going forward";
        QString direction = "forward";
        move(direction);
    }
}

