#include "connectmanager.h"

#include <QAbstractSocket>
#include <QVariant>
#include <QObject>
#include <QJsonParseError>
#include <QElapsedTimer>
#include <QTimer>

ConnectManager::ConnectManager(QObject *parent)
{
    this->setParent(parent);

    rawData.clear();

    tcpSocket = new QTcpSocket( this );

    connect(tcpSocket, SIGNAL(connected()), this, SLOT(socketConnected()));
    connect(tcpSocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(errorHandler(QAbstractSocket::SocketError)));
    connect(&connOpenTimer, SIGNAL(timeout()), this, SLOT(timeout()));
    connOpenTimer.setSingleShot(true);

    in.setDevice(tcpSocket);

    connect( tcpSocket, SIGNAL(readyRead()), SLOT(readTcpData()) );

    waitingData = false;
}

void ConnectManager::Connect(QString hostName, quint16 port)
{
    tcpSocket->connectToHost(hostName, port);

    connOpenTimer.start(2000);
}

void ConnectManager::Disconnect()
{
    waitingData = false;

    tcpSocket->abort();
}

void ConnectManager::socketConnected()
{
    connOpenTimer.stop();

    emit connected();
}

void ConnectManager::socketDisconnected()
{
    emit disconnected();
}

void ConnectManager::jsonWrite(QJsonDocument &jsonReq)
{
    if ( !tcpSocket->isOpen() )
    {
        qDebug() << "Conn: Socket not open";
    }

    Q_ASSERT( !waitingData );

    qDebug() << jsonReq.toJson(QJsonDocument::Compact);

    waitingData = true;

    tcpSocket->write(jsonReq.toJson(QJsonDocument::Compact));

    timer.start();
}

void ConnectManager::readTcpData()
{
    QString latencyValue = QString::number(timer.elapsed());
    emit latency(latencyValue);

    QJsonParseError parseError;
    qint64 length = tcpSocket->bytesAvailable();

    if ( length == 0 )
    {
        qDebug() << "Conn: Packet lenght 0";
        return;
    }

    rawData.append(tcpSocket->read(length));

    jsonResp = QJsonDocument::fromJson(rawData, &parseError);

    if ( !( parseError.error == QJsonParseError::NoError ) )
    {
        qDebug() << "Conn: json not valid";
        qDebug() << "Got:" << rawData;
        rawData.clear();
        return;
    }

    rawData.clear();

    qDebug() << jsonResp.toJson(QJsonDocument::Compact);
    waitingData = false;

    emit jsonReceived(jsonResp);
}

void ConnectManager::errorHandler(QAbstractSocket::SocketError socketError)
{
    qDebug() << tcpSocket->errorString();
}

void ConnectManager::timeout()
{
    tcpSocket->abort();
    tcpSocket->error(QAbstractSocket::ConnectionRefusedError);
}
