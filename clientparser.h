#ifndef CLIENT_PARSER_H
#define CLIENT_PARSER_H

#include <QObject>
#include <QJsonDocument>

class ClientParser : public QObject
{
    Q_OBJECT
public:
    explicit ClientParser(QObject *parent = 0);

signals:
    void jsonRequest(QJsonDocument &jsonReq);

    void botJoined(quint64 id);
    void requestFOW(quint64 id);
    void fullFOW(QString &orientation, quint8 width, quint8 height, QString &pattern);
    void partialFOW(QString &orientation, quint8 width, quint8 height, QString &pattern);
    void botCharacterStatus(QString &status, quint32 health);
    void serverError(QString &errorMessage);

public slots:
    void jsonResp(QJsonDocument &jsonResp);

    void reqJoinHandler(QString &nick, QString &password);
    void reqFOWHandler(quint64 id);
    void reqMoveHandler(quint64 id, QString &direction);
    void reqRotateHandler(quint64 id, QString &orientation);
    void reqInteractHandler(quint64 id, quint64 targetID);
    void reqAttackHandler(quint64 id);

private slots:

    void keyHandler(QJsonObject &key);
    void weaponHandler(QJsonObject &weapon);
    void bombHandler(QJsonObject &bomb);

    void itemHandler(QJsonObject &item);
    void visibleItemsHandler(QJsonArray &inventory);

    void humanHandler(QJsonObject &human);

    void characterHandler(QJsonObject &character);
    void visibleCharactersHandler(QJsonArray &characters);

    void sightDataHandler(QJsonObject &sightData);

    void inventoryHandler(QJsonArray &inventory);
    void ownCharacterHandler(QJsonObject &character);

    void respJoinHandler(QJsonObject &respJoin);
    void respQuitHandler(QJsonObject &respQuit);
    void respFOWHandler(QJsonObject &respFOW);
    void respStatusHandler(QJsonObject &respStatus);
    void errorHandler(QJsonObject &error);

private:
    QJsonObject actionReqConstructor(quint64 id);

    QString _nick;
    QString _password;
};

#endif // CLIENT_PARSER_H
