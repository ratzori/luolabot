#ifndef LUOLABOT_H
#define LUOLABOT_H

#include "connectmanager.h"

#include <QMainWindow>
#include <QHBoxLayout>

namespace Ui {
class LuolaBot;
}

class LuolaBot : public QMainWindow
{
    Q_OBJECT

public:
    explicit LuolaBot(QWidget *parent = 0);
    ~LuolaBot();

public slots:
    void fowData(QString &orientation, quint8 width, quint8 height, QString &pattern);

signals:
    void moveRequest(QString &direction);
    void rotationRequest(QString &rotation);
    void joinRequest(QString &nick, QString &password);
    void interactRequest(quint64 targetID);
    void aiActivateRequest(void);
    void aiDeactivateRequest(void);

private slots:
    void connected();

    void disconnected();

    void aiActivated();

    void aiDeactivated();

    void on_buttonForward_clicked();

    void on_buttonBackward_clicked();

    void on_buttonLeft_clicked();

    void on_buttonRight_clicked();

    void on_buttonNorth_clicked();

    void on_buttonEast_clicked();

    void on_buttonSouth_clicked();

    void on_buttonWest_clicked();

    void on_buttonJoin_clicked();

    void on_buttonQuit_clicked();

    void on_buttonInteract_clicked();

    void on_connectButton_clicked();

    void on_aiButton_clicked();

    void serverError(QString &error);

private:
    void assignShortcuts(void);

    Ui::LuolaBot *ui;
    QHBoxLayout *mainLayout;
    ConnectManager *conn;
    bool connectionOpen;
    bool _aiActivated;
};

#endif // LUOLABOT_H
