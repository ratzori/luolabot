#ifndef BOT_H
#define BOT_H

#include <QVector>
#include <QObject>
#include <QMetaObject>
#include <QPoint>

class Bot : public QObject
{
    Q_OBJECT
public:
    explicit Bot(QObject *parent = 0);
    enum BotState { Init, InitScan, scanOngoing, decideAction, goForward, openDoor, closeDoor, turnRight, turnBackwards };
    Q_ENUM(BotState)

private slots:
    void aiTurnOpportunity(int time);
    int orientationToDegree(QString &orientation);
    QString degreeToOrientation(int degree);
    void UltimateBrainUnit(void);

signals:
    void requestJoin(QString &nick, QString &password);
    void requestFOW(quint64 botID);
    void requestDisconnect(quint64 botID);
    void requestMove(quint64 botID, QString &direction);
    void requestRotate(quint64 botID, QString &orientation);
    void requestInteract(quint64 botID, quint64 targetID);
    void requestAttack(quint64 botID);
    void viewFrontUpdated(QString view);
    void aiActived(void);
    void aiDeactivated(void);    
    void botStatus(QString status);
    void botMaxHealth(int health);
    void botHealth(int health);

public slots:
    void join(QString &nick, QString &password);
    void move(QString &direction);
    void rotate(int degree);
    void rotate(QString &orientation);
    void interact(quint64 targetID);
    void attack(void);

    void botJoined(quint64 botID);
    void fullFOW(QString &orientation, quint8 width, quint8 height, QString &pattern);
    void partialFOW(QString &orientation, quint8 width, quint8 height, QString &pattern);
    void botCharacterStatus(QString &newStatus, quint32 newHealth);

    void aiActivate(void);
    void aiDeactivate(void);

private:
    QChar posAt( QPoint &point );
    QString blockDetector(QPoint point );
    void viewDetector();
    QPoint degreeToBlockOffset( int orientation );
    void viewConverter();
    bool isMoveableArea(QString block);

    quint64 id;
    QVector<BotState> botState;
    QString _nick;
    QString _password;

    QString characterStatus;
    int health;

    QVector<QChar> sightdata;

    QString _orientation;
    int _orientationDegree;
    int _previousOrientationDegree;
    QString fullPattern;
    QString partialPattern;

    QString frontView;
    QString backView;
    QString rightView;
    QString leftView;
    QString rightBackView;

    /* Data inside full pattern */
    QPoint fullPatternSize;
    QPoint botPos;
    QPoint frontPos;
    QPoint backPos;
    QPoint rightPos;
    QPoint leftPos;
    QPoint rightBackPos;

    /* Data related to full map */
    QPoint myPosition;

    QPoint partialPatternSize;
    QPoint targetPos;
    bool _aiActived;
};

#endif // BOT_H
